# client-wallet

<h2>IPFS</h2>
<a href='https://ipfs.io/'>IPFS</a> is a peer-to-peer hypermedia protocol
to make the web faster, safer, and more open.


<h2>IPFS to Decenternet Storage Integration</h2>
For Decenternet client wallet, we are using the open source library developed by <a href='https://github.com/richardschneider'>Richard Scneider</a> Each fragments content address will be delivered to the requesting client using IPFS Client API. The client wallet doesn't depend to ipfs daemon in order to become part of the IPFS network.

<h2>Client Wallet</h2>
Decenternet Client Wallet user interface is written in C# .Net. We will be able to finish a mock up of IPFS file upload at the end of this month.



